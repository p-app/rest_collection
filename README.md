## REST Collection

This library provides creating of REST-like API for receiving collections of data on top of [SQLAlchemy](https://www.sqlalchemy.org/) declarative object model.

from ...abc import AbstractApiOperatorRegistry

__all__ = [
    'ApiOperatorRegistry',
]


class ApiOperatorRegistry(AbstractApiOperatorRegistry):
    __slots__ = ()

from rest_collection.container import WeakKeyOrderedDict


class _Key:  # pylint: disable=too-few-public-methods
    pass


def test_weak_key_ordered_dict():
    container = WeakKeyOrderedDict()

    key1 = _Key()
    container[key1] = 4

    assert dict(container) == {key1: 4}

    key2 = _Key()
    container[key2] = 2

    assert dict(container) == {key1: 4, key2: 2}

    assert tuple(container.keys()) == (key1, key2)

    del key1

    assert dict(container) == {key2: 2}
    assert tuple(container.keys()) == (key2,)

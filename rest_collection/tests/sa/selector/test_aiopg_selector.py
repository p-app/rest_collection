from urllib.parse import quote

from pytest import mark, skip

from rest_collection.api import ApiRequest, TablenameAliasMap


@mark.asyncio
@mark.usefixtures('clear_db', 'sample')
async def test_aiopg_selector(registry, postgres_url):
    try:
        # pylint: disable=import-outside-toplevel
        from aiopg.sa import create_engine

    except ImportError:
        skip('aiopg does not installed.')

    # pylint: disable=import-outside-toplevel
    from rest_collection.sa import AiopgSqlalchemySelector

    registry.clear()
    api_request = ApiRequest(
        f'filter={quote("b_list.id is null")}&'
        'with=d_list&'
        'with=b_list.c&'
        'with_strict=false&'
        'order_by=b_list.c.name',
        registry,
    )

    async def select():
        async with create_engine(postgres_url) as sa_engine:
            selector = AiopgSqlalchemySelector.from_api_request(
                sa_engine,
                api_request,
                alias_map=TablenameAliasMap(),
            )
            data_ = await selector.select()
            count_ = await selector.count()
        return tuple(map(dict, data_)), count_

    data, count = await select()

    assert data == (
        {
            'b_a_id': None,
            'b_id': None,
            'b_name': None,
            'c_b_id': None,
            'c_id': None,
            'c_name': None,
            'd_a_id': 2,
            'd_id': 2,
            'd_name': 'name 2',
            'id': 2,
            'name': 'name 2',
        },
    )
    assert count == 1

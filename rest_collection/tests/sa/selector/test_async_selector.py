from operator import methodcaller
from urllib.parse import quote

from pytest import mark, skip
from sqlalchemy.engine import URL, make_url
from sqlalchemy.ext.asyncio import create_async_engine

from rest_collection.api import ApiRequest, TablenameAliasMap
from rest_collection.sa import AsyncSqlalchemySelector


@mark.asyncio
@mark.usefixtures('clear_db', 'sample')
async def test_async_selector(registry, postgres_url):
    try:
        # pylint: disable=unused-import,import-outside-toplevel
        import asyncpg
    except ImportError:
        skip('asyncpg does not installed.')

    registry.clear()
    api_request = ApiRequest(
        f'filter={quote("b_list.id is null")}&'
        'with=d_list&'
        'with=b_list.c&'
        'with_strict=false&'
        'order_by=b_list.c.name',
        registry,
    )

    async def select():
        url: URL = make_url(postgres_url)
        sa_engine = create_async_engine(
            url.set(drivername='postgresql+asyncpg'),
        )

        try:
            selector = AsyncSqlalchemySelector.from_api_request(
                sa_engine,
                api_request,
                alias_map=TablenameAliasMap(),
            )
            data_ = await selector.select()
            count_ = await selector.count()
            return tuple(map(methodcaller('_asdict'), data_)), count_

        finally:
            await sa_engine.dispose()

    data, count = await select()

    assert data == (
        {
            'b_a_id': None,
            'b_id': None,
            'b_name': None,
            'c_b_id': None,
            'c_id': None,
            'c_name': None,
            'd_a_id': 2,
            'd_id': 2,
            'd_name': 'name 2',
            'id': 2,
            'name': 'name 2',
        },
    )
    assert count == 1

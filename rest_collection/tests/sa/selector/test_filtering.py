from urllib.parse import quote

from pytest import mark


@mark.usefixtures('clear_db', 'sample')
def test_selector_filtering(select):
    # Тестирование фильтрации по C
    select_result = select(f'filter={quote("b_list.c.name eq name 2")}')
    assert select_result.data == (
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'id': 1,
            'name': 'name 1',
        },
    )

    select_result = select(
        f'filter={{{quote("b_list.c.name ne name 2")}}} and '
        f'{{{quote("b_list.c.name ne name 4")}}} and '
        f'{{{quote("b_list.c.name ne name 6")}}} and '
        f'{{{quote("b_list.c.name ne name 8")}}}',
    )
    assert select_result.data == (
        {
            'b_a_id': 3,
            'b_id': 5,
            'b_name': 'name 2',
            'c_b_id': 5,
            'c_id': 5,
            'c_name': 'name 10',
            'id': 3,
            'name': 'name 3',
        },
        {
            'b_a_id': 3,
            'b_id': 5,
            'b_name': 'name 2',
            'c_b_id': 5,
            'c_id': 6,
            'c_name': 'name 12',
            'id': 3,
            'name': 'name 3',
        },
    )

    # Тестирование фильтрации по F
    select_result = select(f'filter={quote("d_list.f_many.name eq name 2")}')
    assert select_result.data == (
        {
            'd_a_id': 2,
            'd_id': 2,
            'd_name': 'name 2',
            'f_id': 5,
            'f_name': 'name 2',
            'id': 2,
            'name': 'name 2',
        },
    )

    select_result = select(
        f'filter={quote("b_list.g_list.b_id eq 2")}',
    )
    assert select_result.data == (
        {
            'b_a_id': 1,
            'b_id': 2,
            'b_name': 'name 5',
            'g_another_b_id': 3,
            'g_b_id': 2,
            'id': 1,
            'name': 'name 1',
        },
    )

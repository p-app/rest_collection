from json import dumps
from urllib.parse import quote

from pytest import mark

from rest_collection.api import NumericAliasMap


@mark.usefixtures('clear_db', 'sample')
def test_selector_sorting_filtering(select):
    # Тестирование фильтрации по C, сортировки по F
    filter_ = quote(f'b_list.c.name in {dumps(["name 2", "name 10"])}')
    select_result = select(
        f'filter={filter_}&order_by=d_list.f_many.name',
    )
    assert select_result.data == (
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 4,
            'f_name': 'name 3',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 3,
            'f_name': 'name 4',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 2,
            'f_name': 'name 5',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 1,
            'f_name': 'name 6',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 3,
            'b_id': 5,
            'b_name': 'name 2',
            'c_b_id': 5,
            'c_id': 5,
            'c_name': 'name 10',
            'd_a_id': 3,
            'd_id': 3,
            'd_name': 'name 3',
            'f_id': None,
            'f_name': None,
            'id': 3,
            'name': 'name 3',
        },
    )

    # Тестирование фильтрации по C, сортировки по B
    filter_ = quote(f'b_list.c.name in {dumps(["name 2", "name 10"])}')
    select_result = select(
        f'filter={filter_}&order_by=b_list.name',
    )
    assert select_result.data == (
        {
            'b_a_id': 3,
            'b_id': 5,
            'b_name': 'name 2',
            'c_b_id': 5,
            'c_id': 5,
            'c_name': 'name 10',
            'id': 3,
            'name': 'name 3',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'id': 1,
            'name': 'name 1',
        },
    )

    # Тестирование фильтрации по B, сортировки по C
    filter_ = quote(f'b_list.name in {dumps(["name 2", "name 5"])}')
    select_result = select(
        f'filter={filter_}&order_by=b_list.c.name',
    )
    assert select_result.data == (
        {
            'b_a_id': 3,
            'b_id': 5,
            'b_name': 'name 2',
            'c_b_id': 5,
            'c_id': 5,
            'c_name': 'name 10',
            'id': 3,
            'name': 'name 3',
        },
        {
            'b_a_id': 3,
            'b_id': 5,
            'b_name': 'name 2',
            'c_b_id': 5,
            'c_id': 6,
            'c_name': 'name 12',
            'id': 3,
            'name': 'name 3',
        },
        {
            'b_a_id': 1,
            'b_id': 2,
            'b_name': 'name 5',
            'c_b_id': None,
            'c_id': None,
            'c_name': None,
            'id': 1,
            'name': 'name 1',
        },
    )

    filter_ = quote(f'b_list.name in {dumps(["name 2", "name 5"])}')
    select_result = select(
        f'filter={filter_}&order_by=b_list.c.name&desc=yes',
    )
    assert select_result.data == (
        {
            'b_a_id': 1,
            'b_id': 2,
            'b_name': 'name 5',
            'c_b_id': None,
            'c_id': None,
            'c_name': None,
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 3,
            'b_id': 5,
            'b_name': 'name 2',
            'c_b_id': 5,
            'c_id': 6,
            'c_name': 'name 12',
            'id': 3,
            'name': 'name 3',
        },
        {
            'b_a_id': 3,
            'b_id': 5,
            'b_name': 'name 2',
            'c_b_id': 5,
            'c_id': 5,
            'c_name': 'name 10',
            'id': 3,
            'name': 'name 3',
        },
    )

    # Тестирование сортировки по E, фильтрации по F
    alias_map = NumericAliasMap()
    select_result = select(
        f'filter={quote("d_list.f_many.name gt name 5")}&'
        f'order_by=d_list.e_list.name&'
        f'desc=yes',
        alias_map=alias_map,
    )

    assert dict(alias_map) == {
        'd_list': '_00',
        'd_list.e_list': '_01',
        'd_list.f_many': '_02',
    }
    assert select_result.data == (
        {
            '_00_a_id': 1,
            '_00_id': 1,
            '_00_name': 'name 1',
            '_01_d_id': 1,
            '_01_f_id': 1,
            '_01_name': 'name 6',
            '_02_id': 1,
            '_02_name': 'name 6',
            'id': 1,
            'name': 'name 1',
        },
        {
            '_00_a_id': 1,
            '_00_id': 1,
            '_00_name': 'name 1',
            '_01_d_id': 1,
            '_01_f_id': 2,
            '_01_name': 'name 5',
            '_02_id': 1,
            '_02_name': 'name 6',
            'id': 1,
            'name': 'name 1',
        },
        {
            '_00_a_id': 1,
            '_00_id': 1,
            '_00_name': 'name 1',
            '_01_d_id': 1,
            '_01_f_id': 3,
            '_01_name': 'name 4',
            '_02_id': 1,
            '_02_name': 'name 6',
            'id': 1,
            'name': 'name 1',
        },
        {
            '_00_a_id': 1,
            '_00_id': 1,
            '_00_name': 'name 1',
            '_01_d_id': 1,
            '_01_f_id': 4,
            '_01_name': 'name 3',
            '_02_id': 1,
            '_02_name': 'name 6',
            'id': 1,
            'name': 'name 1',
        },
        {
            '_00_a_id': 2,
            '_00_id': 2,
            '_00_name': 'name 2',
            '_01_d_id': 2,
            '_01_f_id': 1,
            '_01_name': 'name 2',
            '_02_id': 1,
            '_02_name': 'name 6',
            'id': 2,
            'name': 'name 2',
        },
        {
            '_00_a_id': 2,
            '_00_id': 2,
            '_00_name': 'name 2',
            '_01_d_id': 2,
            '_01_f_id': 3,
            '_01_name': 'name 1',
            '_02_id': 1,
            '_02_name': 'name 6',
            'id': 2,
            'name': 'name 2',
        },
        {
            '_00_a_id': 2,
            '_00_id': 2,
            '_00_name': 'name 2',
            '_01_d_id': 2,
            '_01_f_id': 5,
            '_01_name': 'name 0',
            '_02_id': 1,
            '_02_name': 'name 6',
            'id': 2,
            'name': 'name 2',
        },
    )

from json import dumps
from urllib.parse import quote

from pytest import mark

from rest_collection.api import NumericAliasMap


@mark.usefixtures('clear_db', 'sample')
def test_selector_containts_filter(select):
    select_result = select(
        f'filter={quote("b_list.id ct 3")}',
    )
    assert select_result.data == (
        {'b_a_id': 1, 'b_id': 3, 'b_name': 'name 4', 'id': 1, 'name': 'name 1'},
    )


@mark.usefixtures('clear_db', 'sample')
def test_selector(select):
    select_result = select(
        f'filter={quote("{b_list.c.name ew 2} or {b_list.c.name ew 4}")}&'
        'with=d_list&'
        'with=b_list.c&'
        'order_by=b_list.c.name&'
        'desc=true',
    )
    assert select_result.data == (
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 2,
            'c_name': 'name 4',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 3,
            'b_id': 5,
            'b_name': 'name 2',
            'c_b_id': 5,
            'c_id': 6,
            'c_name': 'name 12',
            'd_a_id': 3,
            'd_id': 3,
            'd_name': 'name 3',
            'id': 3,
            'name': 'name 3',
        },
    )
    assert select_result.count == 3

    select_result = select(
        f'filter={quote("{b_list.c.name ew 2} or {b_list.c.name ew 4}")}&'
        'with=d_list&'
        'with=b_list.c&'
        'order_by=b_list.c.name&'
        'desc=true&'
        'start=2&'
        'stop=4',
    )
    assert select_result.data == (
        {
            'b_a_id': 3,
            'b_id': 5,
            'b_name': 'name 2',
            'c_b_id': 5,
            'c_id': 6,
            'c_name': 'name 12',
            'd_a_id': 3,
            'd_id': 3,
            'd_name': 'name 3',
            'id': 3,
            'name': 'name 3',
        },
    )
    assert select_result.count == 3

    select_result = select(
        f'filter={quote("b_list.id is null")}&'
        'with=d_list&'
        'with=b_list.c&'
        'order_by=b_list.c.name',
    )
    assert select_result.data == ()
    assert select_result.count == 0

    select_result = select(
        f'filter={quote("b_list.id is null")}&'
        'with=d_list&'
        'with=b_list.c&'
        'with_strict=false&'
        'order_by=b_list.c.name',
    )
    assert select_result.data == (
        {
            'b_a_id': None,
            'b_id': None,
            'b_name': None,
            'c_b_id': None,
            'c_id': None,
            'c_name': None,
            'd_a_id': 2,
            'd_id': 2,
            'd_name': 'name 2',
            'id': 2,
            'name': 'name 2',
        },
    )
    assert select_result.count == 1

    select_result = select(
        f'filter={quote("b_list.id is null")}&'
        f'with=d_list&'
        f'order_by=b_list.c.name',
    )
    assert select_result.data == (
        {
            'b_a_id': None,
            'b_id': None,
            'b_name': None,
            'c_b_id': None,
            'c_id': None,
            'c_name': None,
            'd_a_id': 2,
            'd_id': 2,
            'd_name': 'name 2',
            'id': 2,
            'name': 'name 2',
        },
    )
    assert select_result.count == 1

    filter_ = quote(f'b_list.c.name bw {dumps(["name 2", "name 6"])}')
    select_result = select(
        f'filter={filter_}&with=d_list&with=b_list.c&order_by=b_list.c.name',
    )
    assert select_result.data == (
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 2,
            'c_name': 'name 4',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 3,
            'b_name': 'name 4',
            'c_b_id': 3,
            'c_id': 3,
            'c_name': 'name 6',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'id': 1,
            'name': 'name 1',
        },
    )
    assert select_result.count == 3

    # Test joining self reference model.
    alias_map = NumericAliasMap()
    select_result = select(
        'with=b_list.another_g_list&with=b_list.g_list',
        alias_map=alias_map,
    )
    # With string joining, we have only one common row.
    assert select_result.data == (
        {
            '_00_a_id': 1,
            '_00_id': 1,
            '_00_name': 'name 6',
            '_01_another_b_id': 1,
            '_01_b_id': 1,
            '_02_another_b_id': 1,
            '_02_b_id': 1,
            'id': 1,
            'name': 'name 1',
        },
    )

    # Another G relation with non strict joining.
    select_result = select(
        'with=b_list.another_g_list&with_strict=false&with=b_list.g_list',
        alias_map=alias_map,
    )
    assert select_result.data == (
        {
            '_00_a_id': 1,
            '_00_id': 1,
            '_00_name': 'name 6',
            '_01_another_b_id': 1,
            '_01_b_id': 1,
            '_02_another_b_id': 1,
            '_02_b_id': 1,
            'id': 1,
            'name': 'name 1',
        },
        {
            '_00_a_id': 1,
            '_00_id': 2,
            '_00_name': 'name 5',
            '_01_another_b_id': None,
            '_01_b_id': None,
            '_02_another_b_id': 3,
            '_02_b_id': 2,
            'id': 1,
            'name': 'name 1',
        },
    )

    assert dict(alias_map) == {
        'b_list': '_00',
        'b_list.another_g_list': '_01',
        'b_list.g_list': '_02',
    }

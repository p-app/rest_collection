from urllib.parse import quote

from pytest import mark

from rest_collection.api import NumericAliasMap


@mark.usefixtures('clear_db', 'sample')
def test_selector_including_filtering(select):
    # Тестирование фильтрации по C, включение B
    select_result = select(
        f'filter={quote("b_list.c.name eq name 2")}&with=b_list',
    )
    assert select_result.data == (
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'id': 1,
            'name': 'name 1',
        },
    )

    select_result = select(
        f'filter={quote("b_list.c.name eq name 2")}&with=b_list.c',
    )
    assert select_result.data == (
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'id': 1,
            'name': 'name 1',
        },
    )

    # Тестирование фильтрации по C, включение C, D, F
    select_result = select(
        f'filter={quote("{b_list.c.name ew 2} or {b_list.c.name ew 4}")}&'
        f'with=d_list&'
        f'with=d_list.f_many&'
        f'with=b_list.c',
    )
    assert select_result.data == (
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 1,
            'f_name': 'name 6',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 2,
            'c_name': 'name 4',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 1,
            'f_name': 'name 6',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 2,
            'f_name': 'name 5',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 2,
            'c_name': 'name 4',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 2,
            'f_name': 'name 5',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 3,
            'f_name': 'name 4',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 2,
            'c_name': 'name 4',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 3,
            'f_name': 'name 4',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 1,
            'c_name': 'name 2',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 4,
            'f_name': 'name 3',
            'id': 1,
            'name': 'name 1',
        },
        {
            'b_a_id': 1,
            'b_id': 1,
            'b_name': 'name 6',
            'c_b_id': 1,
            'c_id': 2,
            'c_name': 'name 4',
            'd_a_id': 1,
            'd_id': 1,
            'd_name': 'name 1',
            'f_id': 4,
            'f_name': 'name 3',
            'id': 1,
            'name': 'name 1',
        },
    )

    # Check for self-reference model joining.
    alias_map = NumericAliasMap()
    select_result = select(
        f'filter={quote("b_list.g_list.b_id eq 2")}&'
        f'with=b_list.another_g_list&'
        f'with_strict=false',
        alias_map=alias_map,
    )
    assert select_result.data == (
        {
            '_00_a_id': 1,
            '_00_id': 2,
            '_00_name': 'name 5',
            # _01 model includes for B1 and B3, but after applying filter,
            # we have only B2 row, that`s why G model for another field is
            # null.
            '_01_another_b_id': None,
            '_01_b_id': None,
            '_02_another_b_id': 3,
            '_02_b_id': 2,
            'id': 1,
            'name': 'name 1',
        },
    )
    assert dict(alias_map) == {
        'b_list': '_00',
        'b_list.another_g_list': '_01',
        'b_list.g_list': '_02',
    }

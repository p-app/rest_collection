from pytest import mark

from rest_collection.api import NumericAliasMap
from rest_collection.sa import SqlalchemyGrouper


@mark.usefixtures('clear_db', 'sample')
def test_grouper(select):
    select_result = select('with=d_list&with=b_list.c')

    grouper = SqlalchemyGrouper.from_select_collection(
        select_result.selector.select_collection,
    )
    assert grouper.group(select_result.raw_data) == {
        'a': (
            {'id': 1, 'name': 'name 1'},
            {'id': 3, 'name': 'name 3'},
        ),
        'b': (
            {'a_id': 1, 'id': 1, 'name': 'name 6'},
            {'a_id': 1, 'id': 3, 'name': 'name 4'},
            {'a_id': 3, 'id': 5, 'name': 'name 2'},
        ),
        'c': (
            {'b_id': 1, 'id': 1, 'name': 'name 2'},
            {'b_id': 1, 'id': 2, 'name': 'name 4'},
            {'b_id': 3, 'id': 3, 'name': 'name 6'},
            {'b_id': 3, 'id': 4, 'name': 'name 8'},
            {'b_id': 5, 'id': 5, 'name': 'name 10'},
            {'b_id': 5, 'id': 6, 'name': 'name 12'},
        ),
        'd': (
            {'a_id': 1, 'id': 1, 'name': 'name 1'},
            {'a_id': 3, 'id': 3, 'name': 'name 3'},
        ),
    }
    assert select_result.count == 6

    select_result = select('with=d_list&order_by=b_list.c.name')

    grouper = SqlalchemyGrouper.from_select_collection(
        select_result.selector.select_collection,
    )
    # Генерируется outerjoin, потому b и c моделей больше
    assert grouper.group(select_result.raw_data) == {
        'a': (
            {'id': 3, 'name': 'name 3'},
            {'id': 1, 'name': 'name 1'},
            {'id': 2, 'name': 'name 2'},
        ),
        'b': (
            {'a_id': 3, 'id': 5, 'name': 'name 2'},
            {'a_id': 1, 'id': 1, 'name': 'name 6'},
            {'a_id': 1, 'id': 3, 'name': 'name 4'},
            {'a_id': 3, 'id': 6, 'name': 'name 1'},
            {'a_id': 1, 'id': 2, 'name': 'name 5'},
            {'a_id': 3, 'id': 4, 'name': 'name 3'},
        ),
        'c': (
            {'b_id': 5, 'id': 5, 'name': 'name 10'},
            {'b_id': 5, 'id': 6, 'name': 'name 12'},
            {'b_id': 1, 'id': 1, 'name': 'name 2'},
            {'b_id': 1, 'id': 2, 'name': 'name 4'},
            {'b_id': 3, 'id': 3, 'name': 'name 6'},
            {'b_id': 3, 'id': 4, 'name': 'name 8'},
        ),
        'd': (
            {'a_id': 3, 'id': 3, 'name': 'name 3'},
            {'a_id': 1, 'id': 1, 'name': 'name 1'},
            {'a_id': 2, 'id': 2, 'name': 'name 2'},
        ),
    }
    assert select_result.count == 10

    select_result = select('with=d_list&with=b_list.c&order_by=b_list.c.name')

    grouper = SqlalchemyGrouper.from_select_collection(
        select_result.selector.select_collection,
    )
    assert grouper.group(select_result.raw_data) == {
        'a': (
            {'id': 3, 'name': 'name 3'},
            {'id': 1, 'name': 'name 1'},
        ),
        'b': (
            {'a_id': 3, 'id': 5, 'name': 'name 2'},
            {'a_id': 1, 'id': 1, 'name': 'name 6'},
            {'a_id': 1, 'id': 3, 'name': 'name 4'},
        ),
        'c': (
            {'b_id': 5, 'id': 5, 'name': 'name 10'},
            {'b_id': 5, 'id': 6, 'name': 'name 12'},
            {'b_id': 1, 'id': 1, 'name': 'name 2'},
            {'b_id': 1, 'id': 2, 'name': 'name 4'},
            {'b_id': 3, 'id': 3, 'name': 'name 6'},
            {'b_id': 3, 'id': 4, 'name': 'name 8'},
        ),
        'd': (
            {'a_id': 3, 'id': 3, 'name': 'name 3'},
            {'a_id': 1, 'id': 1, 'name': 'name 1'},
        ),
    }
    assert select_result.count == 6

    # Group self reference model.
    def group(query_str):
        select_result_ = select(
            query_str,
            alias_map=NumericAliasMap(),
        )
        grouper_ = SqlalchemyGrouper.from_select_collection(
            select_result_.selector.select_collection,
        )
        return grouper_.group(select_result_.raw_data)

    assert group(
        'with=b_list&'
        'with=b_list.another_g_list&with_strict=false&'
        'with=b_list.g_list&with_strict=false',
    ) == {
        'a': (
            {'id': 1, 'name': 'name 1'},
            {'id': 3, 'name': 'name 3'},
        ),
        'b': (
            {'a_id': 1, 'id': 1, 'name': 'name 6'},
            {'a_id': 1, 'id': 2, 'name': 'name 5'},
            {'a_id': 3, 'id': 6, 'name': 'name 1'},
            {'a_id': 3, 'id': 5, 'name': 'name 2'},
            {'a_id': 3, 'id': 4, 'name': 'name 3'},
            {'a_id': 1, 'id': 3, 'name': 'name 4'},
        ),
        'g': (
            {'another_b_id': 1, 'b_id': 1},
            {'another_b_id': 3, 'b_id': 2},
        ),
    }

    assert group(
        'with=b_list.another_g_list&with_strict=false&with=b_list.g_list',
    ) == {
        'a': ({'id': 1, 'name': 'name 1'},),
        'b': (
            {'a_id': 1, 'id': 1, 'name': 'name 6'},
            # It was included B2, because we joining via b_id strictly.
            {'a_id': 1, 'id': 2, 'name': 'name 5'},
        ),
        'g': (
            {'another_b_id': 1, 'b_id': 1},
            {'another_b_id': 3, 'b_id': 2},
        ),
    }

    assert group(
        'with=b_list.another_g_list&with=b_list.g_list&with_strict=false',
    ) == {
        'a': ({'id': 1, 'name': 'name 1'},),
        'b': (
            {'a_id': 1, 'id': 1, 'name': 'name 6'},
            # It was included B#, because we joining via another_b_id
            # strictly.
            {'a_id': 1, 'id': 3, 'name': 'name 4'},
        ),
        'g': (
            {'another_b_id': 1, 'b_id': 1},
            {'another_b_id': 3, 'b_id': 2},
        ),
    }

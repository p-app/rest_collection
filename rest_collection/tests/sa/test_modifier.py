from operator import attrgetter
from urllib.parse import quote

from sqlalchemy import join, outerjoin, select
from sqlalchemy.sql import Join

from rest_collection.api import AliasedTableMap, TablenameAliasMap
from rest_collection.sa import (
    JoinMap,
    SaQueryFilterModifier,
    SaQueryLimitModifier,
    SaQueryOrderByModifier,
    SaQueryRelationModifier,
)
from rest_collection.tests.models import A, B, C


def test_sa_query_limit_modifier(api_limit_factory, join_map):
    api_limit = api_limit_factory('start=12&stop=18')
    modifier = SaQueryLimitModifier(api_limit, join_map)

    query = select(A)
    query = modifier.modify(query)

    assert attrgetter('_offset', '_limit')(query.selectable) == (12, 6)


def test_sa_query_filter_modifier(api_filter_factory, registry, join_map):
    filter_ = quote(
        '{{name ne "2"} or {name ew "te"}} and {name gt 2} or '
        '{b_list.name ne "et"}',
    )
    api_filter = api_filter_factory(
        f'filter={filter_}',
    )
    modifier = SaQueryFilterModifier(api_filter, join_map)
    modifier.fill_join_map()

    query = select(A)
    query = modifier.modify(query)

    assert (
        str(
            getattr(
                query,
                '_whereclause',
            ),
        )
        == '(a.name != :name_1 OR a.name LIKE :name_2) AND '
        'a.name > :name_3 OR _00.name != :name_4'
    )

    assert dict(join_map.aliased_table_map.alias_map) == {
        'b_list': '_00',
    }

    ((api_pointer, join_mode),) = tuple(join_map.items())
    assert join_mode == outerjoin
    assert api_pointer == registry['b_list']


def test_sa_query_order_by_modifier(
    api_order_by_factory,
    registry,
    join_map,
):
    api_order_by = api_order_by_factory(
        'test=1&sort_by=name&desc=true&filter_by=1&'
        'order_by=b_list.c.name&a=2&sort=down&'
        'sort_by=b_list.c.b_id&n=3&sort_by=name',
    )

    query = select(
        A,
    ).select_from(
        join(
            join(A, B, A.id == B.a_id),
            C,
            C.b_id == B.id,
        ),
    )

    modifier = SaQueryOrderByModifier(api_order_by, join_map)
    modifier.fill_join_map()
    query = modifier.modify(query)

    expressions = tuple(
        map(
            str,
            attrgetter('selectable._order_by_clause.clauses')(query),
        ),
    )
    aliased_table_map = join_map.aliased_table_map
    assert expressions == tuple(
        map(
            str,
            (
                A.name.desc(),
                aliased_table_map[registry['b_list.c.name']].c.name.desc(),
                aliased_table_map[registry['b_list.c.b_id']].c.b_id.asc(),
            ),
        ),
    )

    assert tuple(join_map) == (registry['b_list'], registry['b_list.c'])


def test_sa_query_relation_modifier(
    registry,
    api_relation_factory,
    join_map,
):
    api_relation = api_relation_factory('with=b_list.c&with=d_list')

    query = select(A)

    modifier = SaQueryRelationModifier(api_relation, join_map)
    modifier.fill_join_map()
    query = modifier.modify(query)

    rights = []

    def collect_rights(left):
        rights.append(left.right.name)

        if isinstance(left.left, Join):
            collect_rights(left.left)
        else:
            rights.append(left.left.name)

    collect_rights(query.get_final_froms()[0])
    assert set(rights) == {'a', '_00', '_01', '_02'}

    assert dict(join_map.aliased_table_map.alias_map) == {
        'b_list': '_00',
        'b_list.c': '_01',
        'd_list': '_02',
    }

    api_relation = api_relation_factory(
        'with=d_list&with=d_list.f_many',
    )

    assert api_relation.relation_pointers == {
        registry['d_list.f_many']: True,
        registry['d_list']: True,
    }

    query = select(A)
    join_map = JoinMap(AliasedTableMap(TablenameAliasMap()))

    modifier = SaQueryRelationModifier(api_relation, join_map)
    modifier.fill_join_map()
    query = modifier.modify(query)

    rights = []
    collect_rights(query.get_final_froms()[0])
    assert set(rights) == {'a', 'f', 'e', 'd'}

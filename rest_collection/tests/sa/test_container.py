from sqlalchemy import join, outerjoin

from rest_collection.sa import JoinMap


def test_join_map(registry, aliased_table_map):
    join_map = JoinMap(aliased_table_map)
    join_map.add_pointer(registry['b_list.c'])

    assert dict(join_map) == {
        registry['b_list.c']: outerjoin,
        registry['b_list']: outerjoin,
    }

    join_map.add_pointer(registry['b_list.c'], innerjoin=True)

    assert dict(join_map) == {
        registry['b_list.c']: join,
        registry['b_list']: join,
    }

    join_map.add_pointer(registry['b_list.c'], strict_outerjoin=True)

    assert dict(join_map) == {
        registry['b_list.c']: outerjoin,
        registry['b_list']: join,
    }

    join_map = JoinMap(aliased_table_map)
    join_map.add_pointer(registry['d_list.e_list'], innerjoin=True)
    join_map.add_pointer(registry['d_list.e_list.f'], innerjoin=True)

    assert dict(join_map) == {
        registry['d_list']: join,
        registry['d_list.e_list']: join,
        registry['d_list.e_list.f']: join,
    }

    join_map.add_pointer(registry['d_list.e_list'], strict_outerjoin=True)

    assert dict(join_map) == {
        registry['d_list']: join,
        registry['d_list.e_list']: outerjoin,
        registry['d_list.e_list.f']: outerjoin,
    }

    join_map.add_pointer(registry['d_list.e_list'], innerjoin=True)

    assert dict(join_map) == {
        registry['d_list']: join,
        registry['d_list.e_list']: outerjoin,
        registry['d_list.e_list.f']: outerjoin,
    }

from collections import namedtuple
from itertools import chain, repeat
from operator import methodcaller
from typing import (
    Any,
    Callable,
    Iterable,
    Iterator,
    List,
    Mapping,
    Sequence,
    Tuple,
    Type,
)

from pytest import fixture
from sqlalchemy import insert
from sqlalchemy.engine import Connection, Row
from sqlalchemy.sql import ClauseElement

from rest_collection.api import (
    AliasedTableMap,
    ApiRequest,
    NumericAliasMap,
    TablenameAliasMap,
)
from rest_collection.sa import JoinMap, SqlalchemySelector
from rest_collection.tests.models import A, B, C, D, DeclarativeBase, E, F, G


# pylint: disable=redefined-outer-name


def _repeat_factory(quantity: int) -> Callable[[Any], Iterator[Any]]:
    return lambda value: repeat(value, quantity)


def _repeat_members(
    iterable: Iterable[Any],
    quantity: int,
) -> Iterator[Any]:
    return chain.from_iterable(map(_repeat_factory(quantity), iterable))


def _enumerate_repeat_members(
    iterable: Iterable[Any],
    quantity: int,
) -> Iterator[Tuple[int, Any]]:
    return enumerate(_repeat_members(iterable, quantity), start=1)


@fixture()
def sample(sa_engine):
    def sa_execute(query: ClauseElement) -> List[Row]:
        sa_connection: Connection
        with sa_engine.begin() as sa_connection:
            return sa_connection.execute(query).fetchall()

    def add_all(
        sa_cls: Type[DeclarativeBase],
        data: Sequence[Mapping[str, Any]],
        return_id: bool = True,
    ) -> List[Row]:
        return sa_execute(
            insert(
                sa_cls,
            )
            .values(
                data,
            )
            .returning(
                sa_cls.id if return_id else None,
            ),
        )

    # четыре корневых модели:
    #   A1
    #   A2
    #   A3
    #   A4
    a_model_data = add_all(
        A,
        tuple({'id': i, 'name': f'name {i}'} for i in range(1, 5)),
    )
    # sa_execute(
    #     update(
    #         A,
    #     ).where(
    #         A.id == a_model_data[-1]['id'],
    #     ).values(
    #         parent_id=a_model_data[0]['id'],
    #     ),
    # )

    # У каждой второй модели A есть 3 модели B. То есть, образовались цепочки:
    #   A1-B1
    #   A1-B2
    #   A1-B3
    #   A3-B4
    #   A3-B5
    #   A3-B6
    b_model_data = add_all(
        B,
        tuple(
            {
                'id': i,
                'name': f'name {(7 - i)}',
                'a_id': a_item.id,
            }
            for i, a_item in _enumerate_repeat_members(a_model_data[::2], 3)
        ),
    )

    # У каждой второй модели B есть две модели C, то есть, образовались цепочки:
    #   A1-B1-C1
    #   A1-B1-C2
    #   A1-B3-C3
    #   A1-B3-C4
    #   A3-B5-C5
    #   A3-B5-C6
    add_all(
        C,
        tuple(
            {
                'id': i,
                'name': f'name {(i * 2)}',
                'b_id': b_item.id,
            }
            for i, b_item in _enumerate_repeat_members(b_model_data[::2], 2)
        ),
        return_id=False,
    )

    # У первых трех моделей A есть по две модели D. Образованы цепочки:
    #   A1-D1
    #   A2-D2
    #   A3-D3
    d_model_data = add_all(
        D,
        tuple(
            {
                'id': i,
                'name': f'name {i}',
                'a_id': a_item.id,
            }
            for i, a_item in _enumerate_repeat_members(a_model_data[:3], 1)
        ),
    )

    # Есть шесть моделей F:
    #   F1
    #   F2
    #   F3
    #   F4
    #   F5
    #   F6
    f_model_data = add_all(
        F,
        tuple({'id': i, 'name': f'name {(7 - i)}'} for i in range(1, 7)),
    )

    # Первым двум моделям D назначаются отношения к F через модель E.
    # Образованы цепочки:
    #   A1-D1-F1
    #   A1-D1-F2
    #   A1-D1-F3
    #   A1-D1-F4
    #   A2-D2-F1
    #   A2-D2-F3
    #   A2-D2-F5
    add_all(
        E,
        tuple(
            {
                'd_id': d_item.id,
                'f_id': f_item.id,
                'name': f'name {(7 - i)}',
            }
            for i, (d_item, f_item) in enumerate(
                chain(
                    zip(repeat(d_model_data[0]), f_model_data[:4]),
                    zip(repeat(d_model_data[1]), f_model_data[::2]),
                ),
                start=1,
            )
        ),
        return_id=False,
    )

    # Adding relations:
    #   A1-B1-G1
    #   A1-B1-G1 (over another_b_id)
    #   A1-B2-G2
    #   A1-B3-G2 (over another_b_id)
    add_all(
        G,
        (
            {
                'b_id': b_model_data[0].id,
                'another_b_id': b_model_data[0].id,
            },
            {
                'b_id': b_model_data[1].id,
                'another_b_id': b_model_data[2].id,
            },
        ),
        return_id=False,
    )


_SelectResult = namedtuple(
    'SelectResult',
    (
        'data',
        'count',
        'raw_data',
        'api',
        'selector',
    ),
)


@fixture()
def select(registry, sa_engine):
    def do_select(query_str, *args, alias_map=None, **kwargs):
        if alias_map is None:
            alias_map = TablenameAliasMap()

        registry.clear()
        api_request = ApiRequest(query_str, registry)
        selector = SqlalchemySelector.from_api_request(
            sa_engine,
            api_request,
            alias_map=alias_map,
        )
        data = selector.select(*args, **kwargs)
        count = selector.count(*args, **kwargs)
        return _SelectResult(
            tuple(map(methodcaller('_asdict'), data)),
            count,
            data,
            api_request,
            selector,
        )

    return do_select


@fixture()
def alias_map():
    return NumericAliasMap()


@fixture()
def aliased_table_map(alias_map):
    return AliasedTableMap(alias_map)


@fixture()
def join_map(aliased_table_map):
    return JoinMap(aliased_table_map)

import pytest

from rest_collection.api import ApiPointerError
from rest_collection.tests.models import C


def test_query_pointer(registry):
    model = registry['name']
    assert model.sa_column.key == 'name'
    assert registry['name'] == model

    with pytest.raises(ApiPointerError):
        _ = registry['name1']

    model = registry['b_list.name']
    assert model.sa_column.key == 'name'
    assert len(registry) == 3
    assert registry['b_list'].sa_relationship.key == 'b_list'
    assert len(registry['b_list'].childs) == 1

    model = registry['b_list.c.name']
    assert model.parents == (
        registry['b_list'],
        registry['b_list.c'],
    )
    assert model.sa_column.key == 'name'
    assert model.sa_cls == C
    assert len(registry) == 5
    assert registry['b_list.c'].sa_relationship.key == 'c'
    assert len(registry['b_list'].childs) == 2
    assert registry['b_list.c'].parents == (registry['b_list'],)


def test_query_pointer_deep(registry):
    model = registry['b_list.c.name']
    assert model.parents == (
        registry['b_list'],
        registry['b_list.c'],
    )
    assert model.sa_column.key == 'name'
    assert model.sa_cls == C
    assert len(registry) == 3
    assert registry['b_list.c'].sa_relationship.key == 'c'
    assert registry['b_list'].childs == {registry['b_list.c']}
    assert registry['b_list.c'].parents == (registry['b_list'],)

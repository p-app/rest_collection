from pytest import raises

from rest_collection.api import ApiOrderByDirection, ApiOrderByError
from rest_collection.tests.models import A, C


def test_query_order_by(api_order_by_factory, registry):
    with raises(ApiOrderByError):
        # сортировка по связям не может быть выполнена, только по колонкам
        api_order_by_factory(
            'test=1&sort_by=name&desc=true&filter_by=1&'
            'order_by=b_list.c.name&a=2&sort=down&'
            'sort_by=b_list.c&n=3&sort_by=name',
        )

    order_by = api_order_by_factory(
        'test=1&sort_by=name&desc=true&filter_by=1&'
        'order_by=b_list.c.name&a=2&sort=down&'
        'sort_by=b_list.c.b_id&n=3&sort_by=name',
    )
    assert tuple(order_by.items()) == (
        (
            registry['name'],
            ApiOrderByDirection(A.__table__.c.name, False),
        ),
        (
            registry['b_list.c.name'],
            ApiOrderByDirection(C.__table__.c.name, False),
        ),
        (
            registry['b_list.c.b_id'],
            ApiOrderByDirection(C.__table__.c.b_id, True),
        ),
    )

from pytest import raises

from rest_collection.api import ApiRelationError


def test_api_relation(registry, api_relation_factory):
    with raises(ApiRelationError):
        # такой модели нет
        api_relation_factory('with=test')

    with raises(ApiRelationError):
        # колонки использовать нельзя
        api_relation_factory('with=b_list.name')

    relation = api_relation_factory('with=b_list.c&with=d_list')

    assert relation.relation_pointers == {
        registry['b_list.c']: True,
        registry['d_list']: True,
    }

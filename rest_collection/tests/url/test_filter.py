from json import dumps
from operator import attrgetter, eq, gt

from pytest import raises
from sqlalchemy.sql.elements import ClauseList, Null
from sqlalchemy.sql.operators import (
    between_op,
    in_op,
    notilike_op,
    notin_op,
    notlike_op,
)

from rest_collection.api import (
    API_OPERATOR_REGISTRY,
    ApiFilterError,
    ApiFilterExpression,
    ApiFilterExpressionError,
    ApiFilterExpressionGroup,
    ApiFilterOperatorError,
    ApiFilterParser,
)
from rest_collection.tests.models import A


# pylint: disable=comparison-with-callable


def test_operator_registry():
    registry = API_OPERATOR_REGISTRY

    def get_expr(operator_key, value):
        return registry[operator_key](A.name, value)

    def get_expr_data(operator_key, value, clauses_deserializer=None):
        expr = get_expr(operator_key, value)
        right = expr.right

        if isinstance(right, ClauseList):
            value = tuple(map(attrgetter('value'), right.clauses))
        else:
            value = right.value

        return expr.operator, value if not callable(
            clauses_deserializer,
        ) else clauses_deserializer(value)

    assert get_expr_data('eq', 'test') == (eq, 'test')
    assert get_expr_data('gt', 'test') == (gt, 'test')

    # A.name has string type, but null is a reserved word.
    # todo: may be null suits only for "is" and "not is" operators?
    assert isinstance(get_expr('is', dumps(None)).right, Null)

    assert get_expr_data('nn', dumps([1, 2, 3]), set) == (
        # A.name is string type, therefore values are strings.
        notin_op,
        {'1', '2', '3'},
    )
    assert get_expr_data('bw', dumps(['test', 'test2']), tuple) == (
        between_op,
        ('test', 'test2'),
    )

    assert get_expr('new', 'test').operator == notlike_op
    assert get_expr('nie', 'test').operator == notilike_op

    with raises(ApiFilterOperatorError):
        get_expr_data('in', [1, 2, 3])

    result = get_expr_data('in', dumps([1, 2, 3]), tuple)
    assert result[0], set(result[1]) == (
        in_op,
        {'1', '2', '3'},
    )

    assert set(API_OPERATOR_REGISTRY) == {
        'bw',
        'ct',
        'eq',
        'ew',
        'ge',
        'gt',
        'ict',
        'iew',
        'in',
        'is',
        'isw',
        'le',
        'lt',
        'nc',
        'ne',
        'new',
        'nic',
        'nie',
        'nis',
        'nn',
        'ns',
        'nsw',
        'sw',
    }


def test_parser(registry):
    parser = ApiFilterParser(
        registry,
        operator_registry=API_OPERATOR_REGISTRY,
    )

    def check_filter(text, *expressions, conjunction=True):
        group_ = parser.parse(text)
        assert group_ == ApiFilterExpressionGroup(
            *expressions,
            conjunction=conjunction,
        )

    a_name_column = A.__table__.c.name

    group = parser.parse('{name eq 1}')
    assert isinstance(group, ApiFilterExpressionGroup)

    data = group[0]
    assert data.sa_column == a_name_column
    assert data.value == '1'
    assert data.operator == API_OPERATOR_REGISTRY['eq']

    data = parser.parse('{name eq 1} and {name eq 2}')
    assert isinstance(data, ApiFilterExpressionGroup)
    assert data.conjunction is True
    assert tuple(data.expressions) == (
        ApiFilterExpression(
            registry['name'],
            API_OPERATOR_REGISTRY['eq'],
            '1',
        ),
        ApiFilterExpression(
            registry['name'],
            API_OPERATOR_REGISTRY['eq'],
            '2',
        ),
    )

    check_filter(
        '{{name ne "2"} and {name ew "te"}}',
        ApiFilterExpression(
            registry['name'],
            API_OPERATOR_REGISTRY['ne'],
            '"2"',
        ),
        ApiFilterExpression(
            registry['name'],
            API_OPERATOR_REGISTRY['ew'],
            '"te"',
        ),
    )

    check_filter(
        '{{name ne "2"} or {name ew "te"}}',
        ApiFilterExpression(
            registry['name'],
            API_OPERATOR_REGISTRY['ne'],
            '"2"',
        ),
        ApiFilterExpression(
            registry['name'],
            API_OPERATOR_REGISTRY['ew'],
            '"te"',
        ),
        conjunction=False,
    )

    check_filter(
        '{{name ne "2"} or {name ew "te"}} and {name gt 2}',
        ApiFilterExpressionGroup(
            ApiFilterExpression(
                registry['name'],
                API_OPERATOR_REGISTRY['ne'],
                '"2"',
            ),
            ApiFilterExpression(
                registry['name'],
                API_OPERATOR_REGISTRY['ew'],
                '"te"',
            ),
            conjunction=False,
        ),
        ApiFilterExpression(
            registry['name'],
            API_OPERATOR_REGISTRY['gt'],
            '2',
        ),
    )

    check_filter(
        '{{name ne "2"} or {name ew "te"}} and {name gt 2} or '
        '{b_list.name ne "et"}',
        ApiFilterExpressionGroup(
            ApiFilterExpressionGroup(
                ApiFilterExpression(
                    registry['name'],
                    API_OPERATOR_REGISTRY['ne'],
                    '"2"',
                ),
                ApiFilterExpression(
                    registry['name'],
                    API_OPERATOR_REGISTRY['ew'],
                    '"te"',
                ),
                conjunction=False,
            ),
            ApiFilterExpression(
                registry['name'],
                API_OPERATOR_REGISTRY['gt'],
                '2',
            ),
        ),
        ApiFilterExpression(
            registry['b_list.name'],
            API_OPERATOR_REGISTRY['ne'],
            '"et"',
        ),
        conjunction=False,
    )

    # testing curly braces escape
    check_filter(
        '{name eq \\}1}',
        ApiFilterExpression(
            registry['name'],
            API_OPERATOR_REGISTRY['eq'],
            '}1',
        ),
    )

    check_filter(
        '{name eq 1\\\\ }',  # space after filter value is required
        ApiFilterExpression(
            registry['name'],
            API_OPERATOR_REGISTRY['eq'],
            '1\\',
        ),
    )


def test_query_filter(api_filter_factory, registry):
    with raises(ApiFilterError) as err:
        api_filter_factory('filter={{1 eq 2 }')

    assert str(err.value) == 'Incorrect filter \'{{1 eq 2 }\' value.'

    with raises(ApiFilterExpressionError) as err:
        api_filter_factory('filter={{s eq te}}')

    assert str(err.value) == 'There is no model pointer with key \'s\'.'

    with raises(ApiFilterExpressionError) as err:
        api_filter_factory('filter={{b_list.a_id ss te}}')

    assert (
        str(err.value) == 'There is not operator \'ss\' in operator '
        'registry list.'
    )

    filter_ = api_filter_factory('filter={name eq te}&filter={name eq et}')

    assert filter_.data == ApiFilterExpressionGroup(
        ApiFilterExpression(
            registry['name'],
            API_OPERATOR_REGISTRY['eq'],
            'te',
        ),
    )

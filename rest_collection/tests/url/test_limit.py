from pytest import raises

from rest_collection.api import ApiLimitError


def test_query_limit(api_limit_factory):
    with raises(ApiLimitError):
        # в параметрах должны быть только цифры
        api_limit_factory(
            'start=1&stop=t',
        )

    with raises(ApiLimitError):
        # начальная позиция должна быть больше конечной
        api_limit_factory(
            'start=1&stop=0',
        )

    limit = api_limit_factory('start=12&stop=18')
    assert limit.start == 12
    assert limit.stop == 18
    assert limit.limit == 6

    assert not api_limit_factory('')
    assert not api_limit_factory('start=0')
    assert api_limit_factory('start=10')

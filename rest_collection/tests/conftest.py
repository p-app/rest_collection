from asyncio import get_event_loop, new_event_loop, sleep
from functools import partial
from secrets import token_hex

from aiodocker import Docker
from pytest import fixture
from sqlalchemy import MetaData, engine_from_config, select, text
from sqlalchemy.engine import Engine
from sqlalchemy.exc import DatabaseError

from rest_collection.api import (
    AbstractApiRequest,
    ApiFilterRequestContext,
    ApiLimitRequestContext,
    ApiOrderByRequestContext,
    ApiPointerRegistry,
    ApiRelationRequestContext,
)
from .models import A, DeclarativeBase

pytest_plugins = ('pytest_utils',)


# pylint: disable=redefined-outer-name


@fixture(scope='session')
def event_loop():
    loop = new_event_loop()
    yield loop
    loop.close()


@fixture(scope='session')
def postgres_docker_container_port(unused_tcp_port_factory):
    """postgres docker container host machine port."""
    return unused_tcp_port_factory()


@fixture(scope='session')
async def postgres_docker_container(postgres_docker_container_port):
    async with Docker() as docker_client:
        container = await docker_client.containers.run(
            {
                'Image': 'postgres',
                'PortBindings': {
                    '5432/tcp': [
                        {'HostPort': str(postgres_docker_container_port)},
                    ],
                },
                'Env': ['POSTGRES_PASSWORD=postgres'],
            },
            name=f'rest_collection_postgres_{token_hex(nbytes=3)}',
        )
        yield container
        await container.delete(v=True, force=True)


@fixture(scope='session')
def postgres_url(
    postgres_docker_container,  # pylint: disable=unused-argument
    postgres_docker_container_port,
):
    return (
        f'postgresql://postgres:postgres@localhost:'
        f'{postgres_docker_container_port}/postgres'
    )


@fixture(scope='session')
async def sa_engine(request, postgres_url):
    engine: Engine = engine_from_config(
        {'sqlalchemy.url': postgres_url},
        future=True,
    )

    def check_connection():
        try:
            with engine.connect() as connection:
                connection.execute(select())
            return True
        except DatabaseError:
            return False

    run_in_executor = partial(get_event_loop().run_in_executor, None)
    while not await run_in_executor(check_connection):
        await sleep(0.1)

    metadata: MetaData = DeclarativeBase.metadata
    await run_in_executor(partial(metadata.create_all, bind=engine))
    request.addfinalizer(partial(metadata.drop_all, bind=engine))
    return engine


@fixture(autouse=True)
def clear_db(sa_engine):
    # очистка таблиц непосредственно в БД
    with sa_engine.begin() as sa_connection:
        for table in reversed(DeclarativeBase.metadata.sorted_tables):
            sa_connection.execute(
                text(
                    f'TRUNCATE {table.name} CASCADE',
                ),
            )


@fixture()
def request_qs():
    class DummyRequest:  # pylint: disable=too-few-public-methods
        def __init__(self, query_string):
            self.query_string = query_string

    return DummyRequest


class _ApiRequest(AbstractApiRequest):
    @property
    def filter(self):  # noqa
        pass

    @property
    def limit(self):  # noqa
        pass

    @property
    def order_by(self):  # noqa
        pass

    @property
    def relation(self):  # noqa
        pass


@fixture()
def api_cls():
    return _ApiRequest


@fixture()
def registry():
    return ApiPointerRegistry(A)


@fixture()
def api_factory(api_cls, registry):
    return lambda query_str: api_cls(query_str, registry)


@fixture()
def api_data_type_factory(api_factory):
    def get_data_type(cls):
        return lambda query_str: cls(api_factory(query_str))

    return get_data_type


@fixture()
def api_limit_factory(api_data_type_factory):
    return api_data_type_factory(ApiLimitRequestContext)


@fixture()
def api_filter_factory(api_data_type_factory):
    return api_data_type_factory(ApiFilterRequestContext)


@fixture()
def api_relation_factory(api_data_type_factory):
    return api_data_type_factory(ApiRelationRequestContext)


@fixture()
def api_order_by_factory(api_data_type_factory):
    return api_data_type_factory(ApiOrderByRequestContext)

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import declarative_base, relationship

__all__ = [
    'DeclarativeBase',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
]


DeclarativeBase = declarative_base()


# pylint: disable=invalid-name,too-few-public-methods


class A(DeclarativeBase):
    __tablename__ = 'a'

    id = Column(Integer, primary_key=True)
    name = Column(String)


class B(DeclarativeBase):
    __tablename__ = 'b'

    id = Column(Integer, primary_key=True)
    a_id = Column(Integer, ForeignKey('a.id'))
    name = Column(String)

    a = relationship('A', backref='b_list')


class C(DeclarativeBase):
    __tablename__ = 'c'

    id = Column(Integer, primary_key=True)
    b_id = Column(Integer, ForeignKey('b.id'))
    name = Column(String)

    c = relationship('B', backref='c', uselist=False)


class D(DeclarativeBase):
    __tablename__ = 'd'

    id = Column(Integer, primary_key=True)
    a_id = Column(Integer, ForeignKey('a.id'))
    name = Column(String)

    a = relationship('A', backref='d_list')


class E(DeclarativeBase):
    __tablename__ = 'e'

    d_id = Column(Integer, ForeignKey('d.id'), primary_key=True)
    f_id = Column(Integer, ForeignKey('f.id'), primary_key=True)
    name = Column(String)

    d = relationship('D', backref='e_list')
    f = relationship('F', backref='f_list')


class F(DeclarativeBase):
    __tablename__ = 'f'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    d_many = relationship(
        'D',
        secondary=E.__table__,
        backref='f_many',
        viewonly=True,
    )


class G(DeclarativeBase):
    __tablename__ = 'g'

    b_id = Column(Integer, ForeignKey('b.id'), primary_key=True)
    another_b_id = Column(Integer, ForeignKey('b.id'), primary_key=True)

    b = relationship('B', backref='g_list', foreign_keys=[b_id])
    another_b = relationship(
        'B',
        backref='another_g_list',
        foreign_keys=[another_b_id],
    )
